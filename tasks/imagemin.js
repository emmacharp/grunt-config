module.exports = {
	 test: {
	  options: {
	    optimizationLevel: 7
	  },
	  files: [{
				expand: true,
	   cwd: '<%= path.images %>/',
	   src: '**/*.{png,jpg,jpeg,gif}',
				dest: 'tmp'
	  }]
	 }
};
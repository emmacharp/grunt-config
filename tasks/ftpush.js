module.exports = {
	  build: {
	    auth: {
	      host: '<%= ftp.host %>',
	      port: '<%= ftp.port %>',
	      authKey: '<%= ftp.authKey %>'
	    },
	    src: '<%= ftp.src %>',
	    dest: '<%= ftp.dest %>',
	    exclusions: ['**/.DS_Store', '**/Thumbs.db', '**/.git', '**/.git/*', '.gitignore', '*.sublime-*', '**/sass/**']
	    //keep: ['/important/images/at/server/*.jpg']
	  }
};
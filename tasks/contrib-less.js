module.exports = {
		main: {
						files: {
			      "<%= path.styles %>/css/main.css": ["<%= path.styles %>/less/main.less"]
						}
					},
		ie: {
						files: {
			      "<%= path.styles %>/css/ie.css": ["<%= path.styles %>/less/ie.less"]
						}
					}
};
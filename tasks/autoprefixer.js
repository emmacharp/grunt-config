module.exports = {
		options: {
					browsers: ['last 3 versions', 'ie 8', 'ie 7', 'opera 12.1']
    },
				main: {
      src: "<%= path.styles %>/css/main.css"
    }
};
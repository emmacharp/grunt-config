module.exports = {
			css: {
				files: ["<%= path.styles %>/sass/**/*"],
				tasks: ['sass:main']
			},
			
			reloading: {
				files: ["dev/**/*", "!<%= path.styles %>/sass/**/*"],
				options: { livereload: true }
			}
};
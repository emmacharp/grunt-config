module.exports = {
  icons: {
    src: '<%= path.icons %>/*.svg',
    dest: '<%= path.icons %>/'
  }
};
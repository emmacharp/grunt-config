module.exports = {
    default: {
      options: {
         cssFormat: "dash"
      },
      src: "dev/assets/internal/config-test.yml",
      dest: [
          "<%= path.styles %>/sass/config-test.scss",
          "<%= path.scripts %>/config-test.js"
      ]
    }
};
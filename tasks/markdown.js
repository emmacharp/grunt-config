module.exports = {
	all: {
		files: [{
   expand: true,
   src: 'dev/content/**/*.md',
   dest: './',
   ext: '.html'
  }],
  options: {
        template: '/Users/Emma/Sites/-CONFIG-/grunt/template.jst',
        preCompile: function(src, context) {},
        postCompile: function(src, context) {},
        templateContext: {},
        markdownOptions: {
          gfm: true,
          sanitize:true,
          highlight: 'manual',
          codeLines: {
            before: '<span>',
            after: '</span>'
          }
        }
      }
 }
};
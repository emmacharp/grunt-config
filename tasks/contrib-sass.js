module.exports = {
		main: {
						files: {
			      "<%= path.styles %>/css/main.css": ["<%= path.styles %>/sass/main.scss"]
						}
					},
		ie: {
						files: {
			      "<%= path.styles %>/css/ie.css": ["<%= path.styles %>/sass/ie.scss"]
						}
					}
};
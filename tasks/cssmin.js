module.exports = {
		main: {
   options: { 
   	banner: '/* \n Theme Name: <%= package.name %> \n Theme URI: <%= package.homepage %> \n Description: The Theme of <%= package.title %>. \n Author: <%= package.author %> \n Version: <%= package.version %>\n License: GNU General Public License v2 or later \n License URI: http://www.gnu.org/licenses/gpl-2.0.html \n Tags: \n*/\n'
				},
				files: {
     '<%= path.styles %>/css/main.css': '<%= path.styles %>/css/main.css'
				}
		},

		ie: {
   options: { 
   	banner: '/* \n Theme Name: <%= package.name %> \n Theme URI: <%= package.homepage %> \n Description: The Theme of <%= package.title %> — for deprecated browsers. \n Author: <%= package.author %> \n Version: <%= package.version %>.deprec\n License: GNU General Public License v2 or later \n License URI: http://www.gnu.org/licenses/gpl-2.0.html \n Tags: \n*/\n'
				},
				files: {
     '<%= path.styles %>/css/ie.css': '<%= path.styles %>/css/ie.css'
				}
		}
};